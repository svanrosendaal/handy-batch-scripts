@ECHO OFF

:: Backup the current config file
echo "Backing up phpMyAdmin config file"
copy C:\xampp\phpMyAdmin\config.inc.php C:\xampp\config.inc.php
:: Delete all files in the phpMyAdmin folder
echo "Deleting contents of the phpMyAdmin folder"
cd /d C:\xampp\phpMyAdmin
for /F "delims=" %%i in ('dir /b') do (rmdir "%%i" /s/q || del "%%i" /s/q)

:: Download the latest phpMyAdmin release and unzip
echo "Downloading latest phpMyAdmin release"
curl --location https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.zip --output phpMyAdmin-latest-all-languages.zip
echo "Unzipping phpMyAdmin"
Call :UnZipFile "C:\xampp\phpMyAdmin" "C:\xampp\phpMyAdmin\phpMyAdmin-latest-all-languages.zip"

echo "Cleaning up"
del phpMyAdmin-latest-all-languages.zip
for /f "delims=" %%i in ('dir /b') do (
    echo "Copying contents of %%i to C:\xampp\phpMyAdmin"
    xcopy /s "%%i" C:\xampp\phpMyAdmin
    del /s /q %%i
    rmdir /s /q %%i
)
echo "Reapplying and deleting backed up config"
xcopy C:\xampp\config.inc.php C:\xampp\phpMyAdmin\config.inc.php -Y
del C:\xampp\config.inc.php
echo "DONE!"
exit /b

:UnZipFile <ExtractTo> <newzipfile>
set vbs="%temp%\_.vbs"
if exist %vbs% del /f /q %vbs%
>%vbs%  echo Set fso = CreateObject("Scripting.FileSystemObject")
>>%vbs% echo If NOT fso.FolderExists(%1) Then
>>%vbs% echo fso.CreateFolder(%1)
>>%vbs% echo End If
>>%vbs% echo set objShell = CreateObject("Shell.Application")
>>%vbs% echo set FilesInZip=objShell.NameSpace(%2).items
>>%vbs% echo objShell.NameSpace(%1).CopyHere(FilesInZip)
>>%vbs% echo Set fso = Nothing
>>%vbs% echo Set objShell = Nothing
cscript //nologo %vbs%
if exist %vbs% del /f /q %vbs%'